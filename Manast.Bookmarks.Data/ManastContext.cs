﻿using Manast.Bookmarks.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manast.Bookmarks.Data
{
    public class ManastContext : DbContext
    {
        public ManastContext([NotNull] DbContextOptions options) : base(options)
        {

        }

        public DbSet<Bookmark> Bookmarks { get; set; }
        public DbSet<UrlEntry> UrlEntries { get; set; }
    }
}
