﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Manast.Bookmarks.Model
{
    public class Bookmark
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        
        [JsonPropertyName("color")]
        public string Color { get; set; }
        
        [JsonPropertyName("category")]
        public string Category { get; set; }
        
        [JsonPropertyName("urls")]
        public List<UrlEntry> Urls { get; set; }
    }
}
