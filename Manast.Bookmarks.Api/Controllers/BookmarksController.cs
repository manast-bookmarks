﻿using Manast.Bookmarks.Data;
using Manast.Bookmarks.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Manast.Bookmarks.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookmarksController : ControllerBase
    {
        private readonly ManastContext _context;
        
        public BookmarksController(ManastContext context)
        {
            _context = context;
        }
        
        [HttpGet("/")]
        [ProducesResponseType(typeof(List<Bookmark>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBookmarks()
        {
            try
            {
                var bookmarks = await _context.Bookmarks.Include(x => x.Urls).ToListAsync();

                if (bookmarks.Any())
                    return Ok(bookmarks);
                else
                    return NotFound();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
        
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Bookmark), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBookmark(long id)
        {
            try
            {
                var bookmark = await _context.Bookmarks.Include(x => x.Urls).SingleOrDefaultAsync(x => x.Id == id);
                if (bookmark == null)
                    return NotFound();
                else
                {
                    return Ok(bookmark);
                }
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPost("/")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddBookmark([FromBody] Bookmark bookmark)
        {
            try
            {
                await _context.AddAsync(bookmark);
                await _context.SaveChangesAsync().ConfigureAwait(false);

                return CreatedAtAction(nameof(GetBookmark), new { id = bookmark.Id }, bookmark);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateBookmark(long id, [FromBody] Bookmark bookmark)
        {
            bookmark.Id = id;
            _context.Entry(bookmark).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteBookmark(long id)
        {
            var bookmark = new Bookmark { Id = id };
            _context.Entry(bookmark).State = EntityState.Deleted;
            try
            {
                await _context.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
